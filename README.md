# CC6240 - Tópicos Avançados de Bancos de Dados

Este repositório contém o material usado nas aulas de Tópicos Avançados de Banco de Dados (CC6240) no Centro Universitário FEI. O material de teoria foi escrito usando o [Marp](https://marp.app/) como base, mas pode ser visualizado em outros lugares com uma possível perda de formatação.

## Organização do repositório

O repositório contém os seguintes arquivos e pastas:
- `README.md`: este arquivo, explicando o repositório e o funcionamento da disciplina;
- `cronogramas`: pasta contendo os cronogramas para todas as turmas atualmente cursando a disciplina;
- `aulas`: pasta contendo todos o material das aulas de teoria que pode ser visualizado corretamente usando o [Marp](https://marp.app/);
- `src`: pasta com os códigos desenvolvidos para os assuntos tratados nas aulas. Dentro desta pasta também estão os arquivos:
    - `docker-compose.yaml`: descrição dos containers que podem ser usados para o desenvolvimento do projeto;
    - `shell.nix`: para usar com o [Nix ou NixOS](https://nixos.org/);
    - `.envrc`: para carregar os pacotes listados em `shell.nix` assim que entrar na pasta.

## Funcionamento da disciplina

- Início do semestre: desenvolvimento da proposta do projeto e introdução a bancos NoSQL
- Meio do semestre: desenvolvimento do projeto
- Fim do semestre: desenvolvimento do semestre e tópicos sobre funcionamento de bancos de dados

## Avaliação

### Projeto

- Todos os projetos possuem prazo para entrega
- Todos os projetos devem usar sistema de versionamento (i.e., git)
- Teremos aulas para desenvolvimento dos projetos, mas podem ser feitos em qualquer horário
- Não será necessário apresentar o projeto em sala, apenas entregar o que for pedido
- Os critérios de avaliação estarão junto com o enunciado do projeto

### Provas

2 provas feitas no papel e sem consulta:
- P1: ainda no primeiro mês de aula (marcado no cronograma) e trata apenas sobre o projeto
- P2: será marcada pela secretaria na semana de avaliações finais e trata sobre o projetos e os temas vistos no final do semestre

### Critério de avaliação

$$ \textrm{Média} = Projeto \cdot \frac{0.4 \cdot P1 + 0.6 \cdot P2}{10} $$

- A prova substitutiva substitui apenas a P2 e **não** substitui P1 ou projeto

## Recursos usados para as aulas

- Nos laboratórios da FEI:
    - pgAdmin
    - DBeaver

- Disponível para todos com o email da FEI:
    - JetBrains DataSpell
    - JetBrains DataGrip

- Mas podem usar o que acharem melhor
