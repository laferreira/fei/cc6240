# Cronograma vespertino

| **Data**  | **Dia semana**   | **Aula**    | **Assunto**                              |
| :-------: | :--------------: | :---------: | :--------------------------------------- |
| 06/02     | quinta           | Lab         | Apresentação do projeto do semestre      |
| 07/02     | sexta            | Teoria      | Banco de dados NoSQL                     |
| 13/02     | quinta           | Lab         | Desenvolvimento da proposta do projeto   |
| 14/02     | sexta            | Teoria      | Outras formas de armazenamento de dados  |
| 20/02     | quinta           | Lab         | Desenvolvimento da proposta do projeto   |
| 21/02     | sexta            | Teoria      | Distributed Query Engine                 |
| 27/02     | quinta           | Lab         | Desenvolvimento da proposta do projeto   |
| 28/02     | sexta            | Teoria      | Atividade sobre a proposta do projeto    |
| 06/03     | quinta           | Lab         | Desenvolvimento do projeto               |
| 07/03     | sexta            | Teoria      | Desenvolvimento do projeto               |
| 13/03     | quinta           | Lab         | Desenvolvimento do projeto               |
| 14/03     | sexta            | Teoria      | Desenvolvimento do projeto               |
| 20/03     | quinta           | Lab         | Desenvolvimento do projeto               |
| 21/03     | sexta            | Teoria      | Desenvolvimento do projeto               |
| 27/03     | quinta           | Lab         | Desenvolvimento do projeto               |
| 28/03     | sexta            | Teoria      | Desenvolvimento do projeto               |
| 03/04     | quinta           | Lab         | Desenvolvimento do projeto               |
| 04/04     | sexta            | Teoria      | Desenvolvimento do projeto               |
| 10/04     | quinta           | Lab         | Índices, árvores e hashing               |
| 11/04     | sexta            | Teoria      | Desenvolvimento do projeto               |
| 17/04     | quinta           | Lab         | **Recesso**                              |
| 18/04     | sexta            | Teoria      | **Recesso**                              |
| 24/04     | quinta           | Lab         | Processamento de consultas               |
| 25/04     | sexta            | Teoria      | Desenvolvimento do projeto               |
| 01/05     | quinta           | Lab         | **Recesso**                              |
| 02/05     | sexta            | Teoria      | **Recesso**                              |
| 08/05     | quinta           | Lab         | Otimização de consultas                  |
| 09/05     | sexta            | Teoria      | Desenvolvimento do projeto               |
| 15/05     | quinta           | Lab         | Transações                               |
| 16/05     | sexta            | Teoria      | Desenvolvimento do projeto               |
| 22/05     | quinta           | Lab         | Concorrência e Recuperação               |
| 23/05     | sexta            | Teoria      | Desenvolvimento do projeto               |

