# Cronograma e plano de estudos orientação

## Semana 1 (08/02): Índices, árvores e hashing
- https://youtu.be/ImSzz8nAZck?si=JQhQRShchzoCWSgC
- https://youtu.be/ZmyC2ss5vXw?si=BUYLMOEuyN3TNSfY
- https://youtu.be/rkMFoAZWbpc?si=PmM46ndgApbc-7FO

## Semana 2 (15/02): Processamento de consultas
- https://youtu.be/u5q2s6WZzTo?si=Ns3iRP2IeSiR4XDy
- https://youtu.be/gsXiln_7i6A?si=6BQnWhTqsZkBzqn7
- https://youtu.be/L5cvNoxu9tE?si=vQgf6VA1HMzEyu8G
- https://youtu.be/VPzEC-_ZQjc?si=-PkrQyWGFQmQ0nBQ
- https://youtu.be/SpWH7O-v1-o?si=IWAtPoz7Lllypz2W
- https://youtu.be/52nmqn40DRA?si=mBysakGtlQ6Sm5EU
- https://youtu.be/hMgVvj7wco4?si=4vaLQjAwq0jGzOqa
- https://youtu.be/15ShDwX_K_A?si=8c45-ZVKqFShz9RJ

## Semana 3 (22/02): Otimização de consultas
- https://youtu.be/0s--yBU3AXo?si=deC42SZkyybTsyqP
- https://youtu.be/41GhUcoP0BQ?si=zgSsFadiuwwPx90-
- https://youtu.be/9YhTIK6vJiw?si=qpAv4KiApFJSGeux

## Semana 4 (01/03): Transações, concorrência e recuperação
- https://youtu.be/CebvaDmKWNU?si=LhZ9Sie7180nv2CL
- https://youtu.be/plpozTa7M9E?si=GnrexiYDIaXBzvwx

## Semana 5 (08/03): concorrência e recuperação
- https://youtu.be/IjNA1cfvEtk?si=zokT8SH6TDjS-c_o
- https://youtu.be/4C6n9RcLuB4?si=Dqlq1yeYCuZOphW_

## Semanas de 6 à 16 (15/03 - 24/05): NoSQL (Document Storage, Wide-column Store, Graph Database e Distributed Query Engine)
- Desenvolvimento dos 3 projetos **individuais**
- https://youtu.be/M6Wi8hz1ur8?si=1-y6gWYKNFXSUuZD
- https://youtu.be/BjZVAbxj1I4?si=aC6gL8fBZipNxZ-r
- https://youtu.be/Q-7oLbGWhB8?si=bMjQPG4stJC0ZZI1
- https://youtu.be/LAJ09pClNlw?si=gIPD8n2u0DEXitrI
- https://youtu.be/HvBvsND_MHY?si=WnxpiUbxOPbu4fPW
- https://youtu.be/HvBvsND_MHY?si=5ygabjq_0dgKvi5k
- https://youtu.be/ryBpeJ4Y93c?si=C4TfluNVL8GCkgyA
- https://youtu.be/WIyD_Yh3730?si=U6y2-Ss6NLkeaxzY
- https://youtu.be/JLdzzsII2sE?si=eN1yXLeLzfWfduZd


