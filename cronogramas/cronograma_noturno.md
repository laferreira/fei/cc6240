# Cronograma noturno

| **Data**  | **Dia semana**   | **Aula**    | **Assunto**                             |
| :-------: | :--------------: | :---------: | :-------------------------------------- |
| 07/02     | sexta            | Lab         | Apresentação do projeto do semestre     |
| 08/02     | sábado           | Teoria      | Desenvolvimento da proposta do projeto  |
| 14/02     | sexta            | Lab         | Banco de dados NoSQL                    |
| 15/02     | sábado           | Teoria      | Desenvolvimento da proposta do projeto  |
| 21/02     | sexta            | Lab         | Distributed Query Engine                |
| 22/02     | sábado           | Teoria      | Desenvolvimento da proposta do projeto  |
| 28/02     | sexta            | Lab         | Outras formas de armazenamento de dados |
| 01/03     | sábado           | Teoria      | Desenvolvimento da proposta do projeto  |
| 07/03     | sexta            | Lab         | Atividade sobre a proposta do projeto   |
| 08/03     | sábado           | Teoria      | Desenvolvimento do projeto              |
| 14/03     | sexta            | Lab         | Desenvolvimento do projeto              |
| 15/03     | sábado           | Teoria      | Desenvolvimento do projeto              |
| 21/03     | sexta            | Lab         | Desenvolvimento do projeto              |
| 22/03     | sábado           | Teoria      | Desenvolvimento do projeto              |
| 28/03     | sexta            | Lab         | Desenvolvimento do projeto              |
| 29/03     | sábado           | Teoria      | Desenvolvimento do projeto              |
| 04/04     | sexta            | Lab         | Desenvolvimento do projeto              |
| 05/04     | sábado           | Teoria      | Desenvolvimento do projeto              |
| 11/04     | sexta            | Lab         | Índices, árvores e hashing              |
| 12/04     | sábado           | Teoria      | Desenvolvimento do projeto              |
| 18/04     | sexta            | Lab         | **Recesso**                             |
| 19/04     | sábado           | Teoria      | **Recesso**                             |
| 25/04     | sexta            | Lab         | Processamento de consultas              |
| 26/04     | sábado           | Teoria      | Desenvolvimento do projeto              |
| 02/05     | sexta            | Lab         | **Recesso**                             |
| 03/05     | sábado           | Teoria      | **Recesso**                             |
| 09/05     | sexta            | Lab         | Otimização de consultas                 |
| 10/05     | sábado           | Teoria      | Desenvolvimento do projeto              |
| 16/05     | sexta            | Lab         | Transações                              |
| 17/05     | sábado           | Teoria      | Desenvolvimento do projeto              |
| 23/05     | sexta            | Lab         | Concorrência e Recuperação              |
| 24/05     | sábado           | Teoria      | Entrega do projeto                      |

