{ pkgs ? import <nixpkgs> {
    config.allowUnfree = true;
} }:
    pkgs.mkShell {
      nativeBuildInputs = with pkgs.buildPackages; [
        dbeaver-bin
        postgresql_15
        mongosh
        mongodb-compass
        cassandra_4
      ];
    }

