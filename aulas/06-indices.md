---
title: 01 - Índices
author: Leonardo Anjoletto Ferreira
theme: custom
paginate: true
lang: pt-br
---

# Índices
## ordenados, BTree e hash

---

## Assuntos

1. Tempos de inserção, busca,...
2. Índices ordenados
3. BTree
4. Hashmap

---

## Exemplos genéricos que vamos usar na aula

* Em qualquer celular atual, mesmo que não seja um smartphone, você pode armazenar contatos. Como podemos gerar os índices para os contatos?

* Se você não tiver um aparelho desse tipo, mas ainda assim está fazendo CC6240, então você já usou o Moodle e sabe que lá é possível ter uma lista de participantes do curso. Como podemos gerar os índices para os participantes dos cursos no Moodle?

---

## Tempos/Complexidade

Lembrando de estrutura de dados:
- Tempo de acesso: tempo gasto para encontrar o (conjunto de) registro(s)
- Tempo de inserção: tempo gasto para inserir um novo item na estrutura
- Tempo de exclusão: tempo gasto para excluir um item
- Espaço adicional: espaço gasto pela estrutura do índice
- Chave de busca: índices usados para a busca

---

## 1. Índices ordenados

---

## Índices ordenados

- Estrutura: chave de busca -> registro de índice
- Índices primários ou de agrupamento:
    - Registros ordenados sequencialmente, como a chave de busca
    - Podem ser densos ou esparsos
    - Podem ser a chave primário, mas não é regra

- Índices secundários ou de não agrupamento: chave de busca é diferente da ordem dos registros

---

## Índices densos

Nos dois exemplos (agenda de telefone e participantes de curso no moodle):
* Cada nome vira uma chave de busca
* Temos um registro de índice para cada valor de chave de busca

---

## Índices densos

![](./figs/indices_densos.png)

---

## Índices densos: inserção

- valor da chave de busca ainda não existe no índice: ele é inserido na posição correta para manter ordenado
- valor da chave já existe:
    - registro de índice aponta para todos os registros: um novo ponteiro é adicionado
    - registro de índice aponta apenas para o primeiro registro: o registro é adicionado e o ponteiro atualizado se for necessário


---

## Índices densos: exclusão

- valor do registro for o único com aquela chave: a chave também é excluída
- registro de índice é feito por ponteiros:
    - Se apontar para todos os registros, o registro e o ponteiro para ele são excluídos
    - Se apontar apenas para o primeiro registro, o registro selecionado é apagado e o ponteiro é atualizado se for necessário

---

## Índices esparsos

Nos dois exemplos:
* Continuamos com cada nome sendo uma chave de busca
* Mas nem todas as chaves de busca viram um registro de índice
* Por exemplo, no lugar de usar a mudança na primeira letra do nome, podemos agrupar por intervalo de letras

---

## Índices esparsos

![](./figs/indices_esparsos.png)

---

## Índices esparsos: inserção

- necessário criar um novo conjunto (bloco): o registro é inserido no primeiro valor da chave de busca
- valor a ser inserido for o menor dentro de um bloco: o ponteiro da registro de índice para a chave de busca é atualizado
- não precisar atualizar o registro de índice: o registro é apenas adicionado

---

## Índices esparsos: exclusão

- índice não estiver em um registro de índice apontando para ele: somente excluímos o índice
- registro não for o único no registro de índice: excluímos o registro e atualizamos o ponteiro
- registro for o único no registro de índice: o registro de índice é atualizado para apontar para o próximo valor. Se o próximo valor já for um índice, a chave é excluída para não ter índices duplicados

---

## Índices densos x esparsos

* índice denso é mais fácil de localizar, porém mais custoso para manter estrutura e fazer leitura

* Solução atual mais comum: índice esparso com uma entrada por índice de bloco
    - Custo de acesso é alto para encontrar o bloco
    - Custo de leitura dentro do bloco é pequeno
    - Melhorias no tempo de acesso e espaço necessário para armazenar o índice

---

## Índices esparsos multiníveis

- índices esparsos são bons enquanto cabem na memória, mas são custosos com grandes quantidades de dados
- uma solução são os índices esparsos multiníveis

---

## Índices secundários

- Devem ser densos
- Tem ponteiro para cada registro:
    - Bloco de registros são ordenados pela chave de busca do índice agrupado
    - Registros de índice secundário não são sequeciais
- Nível extra de indexação: ponteiros apontam para buckets e não diretamente para o arquivo

---

## Índices secundários

![height:500px](./figs/indices_secundarios.png)

---

## Resumindo

![](./figs/resumo_indice.png)

---

## Problemas com índices ordenados

- Performance degrada com o aumento da estrutura do índice
- Solução: mudar a estrutura de organização

---

# 2. BTree

---

## Árvore balanceada

- Vantagens:
    - todos os caminhos da raiz até a folha são do mesmo tamanho
    - Cada nó que não é folha tem entre n/2 e n nós filhos

- Desvantagens:
    - Custo de inserção e exclusão: apesar de tudo, é aceitável
    - Espaços de armazenamento: parte da árvore pode estar vazia

---

## Árvores B

- Mais complexas que as árvores B+
- Poucas vantagens em relação ao espaço usado
- Portando, Árvores-B+ são preferidas pelos desenvolvedores de bancos

---

## Árvore B+

- Cada folha pode armazenar entre $\frac{n-1}{2}$ e $n-1$ valores
- Valores nas folhas não se sobrepõem
- Nós que não são folhas possuem entre $\frac{n}{2}$ e $n$ ponteiros

---

## Árvore B+: exemplo

![](./figs/btree.png)

---

## Árvore B+: consulta
- Custo nunca maior que $log_{n/2}(K)$ em quem $K$ é a quantidade de chaves de busca e $n$ é a quantidade de valores por bloco

![](./figs/btree.png)

---

## Árvore B+: inserção

- Se o nó tiver menos que $n/2$ ponteiros, há redistribuição para garantir balanceamento
- No pior caso, é necessário fazer a divisão do nó raiz e aumentar a altura da árvore


- Se chave de busca existir: adiciona o registro
- Senão:
    - Adiciona a chave
    - Faz o balanceamento
    - Adiciona o registro

---

## Árvore B+: exclusão

- Mesma lógica que inserção:
    - Procura chave
    - Exclui valor
    - Se necessário, faz balanceamento

---

# 3. Hash

---

##  Hashing

Usar hashing permite:
- Não armazenar os dados de forma sequencial
- Permite otimizar os índices
- Ou até mesmo não usar índices

---

## Hash: conceitos

- Os registros serão armazenados em buckets
- K: conjunto de todas as chaves de busca
- B: conjunto de todos os valores de buckets
- h: função hash que mapeia chaves (K) em buckets (B)
- Inserir um registro é calcular o hash do registro e inserir no bucket correto

---

## Hash

- Registros com mesmo hash: armazenados no mesmo bucket (lista de registros)
- Busca, inserção, exclusão:
    - Calcula o hash
    - lê/insere/exclui registro

---

## Hash

Hash pode ser usado para:
1. Organização de arquivo:
    - Registros ficam em buckets
    - Não há índices
2. Organização de índices:
    - Índices em buckets
    - Índices com ponteiros direto para os registros

---

## Hash: organização de arquivos

Dois objetivos:
1. Distribuição uniforme: buckets com a mesma quantidade de chaves
2. Distribuição aleatória: sem complexidade de armazenamento sequencial

Pior função hash: **todas** as chaves no mesmo bucket

---

## Hash: estouro de buckets

* Da forma mais direta: podemos errar na estimativa da quantidade de buckets

* Distorção quando ocorre a distribuição não uniforme dos registros

---

## Hash: overflow de buckets

Esperado:
$$\textrm{número de buckets} = \frac{\textrm{total de registros armazenados}}{\textrm{quantidade de registros que cabem no bucket}} $$

---

## Hash: estouro de buckets
Diminuindo a chance de problemas:
- Fator de Fudge $\sim 0,2$

$$\textrm{número de buckets} = \frac{\textrm{total de registros armazenados}}{\textrm{quantidade de registros que cabem no bucket}} \cdot (1 + \textrm{fator de fudge}) $$

* Ou seja, aproximadamente 20% do espaço dos buckets é perdido

---

## Hash: solução de bucket de overflow

Caso mesmo assim ocorra o overflow, criamos um bucket de overflow...
* Se o bucket estiver cheio, cria-se o bucket de overflow
* Se o bucket de overflow encher, cria-se um novo bucket de overflow
* Junta-se o bucket original com os buckets de overflow em uma lista ligada (encadeamento de overflow)

---

## Hash: estático vs dinâmico

- Problema do estático: definição do número de buckets
- Hashing dinâmico: modificação da função hash levando em consideração as mudanças no banco

---

## Hash expansível

- Divisão/união de buckets conforme a mudança de tamanho do banco:
    - mantém eficiência espacial
    - reorganização apenas nos buckets necessários

---

## Hash expansível: exemplo

* Considere uma função hash que retorna uma sequência de B bits (normalmente, B = 32 bits)
* A criação do bucket usa a sequência desses bits para criar e dividir em novos buckets
* Inicialmente usamos apenas o primeiro bit da sequência para os buckets
* Conforme necessário, dividimos os buckets usando os próximos bits
---

## Hash expansível: exemplo

![](./figs/hash_dinamico.png)
