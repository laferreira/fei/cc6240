---
title: 02 - Processamento de consultas
author: Leonardo Anjoletto Ferreira
theme: custom
paginate: true
lang: pt-br
---

<!--
_header: CC6240 - Tópicos avançados em banco de dados
_footer: Leonardo Anjoletto Ferreira
-->

# Algoritmos de classificação

---

## Classificação

Finalidade de ordenar os dados
1. A consulta pode pedir que os dados venham ordenados (`ORDER BY`)
2. Algumas operações relacionais (e.g. junção) podem ser mais eficientes se os dados estiverem ordenados

---

## Classificação

<div class="columns">
<div>

- Criação de índice sobre atributo a ser ordenado/classificado:
    - operação lógica e não operação física (disco)
    - **problema**: pode acessar mais do que um bloco dependendo da quantidade de registros

</div>

<div>

* Solução: ordenar fisicamente:
    - Se registros na memória principal, técnica conhecida (e.g. quicksort)
    - Se fora da memória pricipal: classificação externa

</div>
</div>

---

## Classificação externa

- sort-merge externo é o mais comum
- Considerando $M$ como a quantidade de blocos que cabem na memória principal
- 2 partes:
    - Ler $M$ blocos, ordenar e salvar em $N$ arquivos temporários
    - Ler um bloco de cada um dos $N$ arquivos e ordenar pelas $N$ tuplas

---

## sort-merge externo - parte 1

```
- i = 0
- repita até encontrar todas as relações:
    1. leia M blocos da relação (todos os blocos se for menor que M)
    2. ordene os dados que estão na memória principal
    3. escreva os dados em um arquivo temporário Ri
    4. i = i + 1
```

---

## sort-merge externo - parte 2

```
- leia um bloco de cada um dos arquivos Ri para uma página de buffer
  na memória
- repita até que todas as páginas de buffer estejam vazias:
    1. escolha a primera tupla (por ordenação) entre todas as que
       foram lidas
    2. escreva esta tupla no resultado
    3. remova a tupla do buffer do arquivo Ri lido
    4. se a página de qualquer buffer estiver vazia e
       não for o fim do arquivo Ri, leia a próxima página do buffer
```
---

## sort-merge externo - custo

- todo bloco da relação é lido e escrito novamente: $2b_r$
- número inicial de arquivos temporários: $\frac{b_r}{M}$
- número de arquivos temporários diminui por um fator de $M - 1$ em cada passo de merge, logo, o número total de passos necessários é $\lceil{log_{M-1}\frac{b_r}{M}}\rceil$
- custo = $b_r \cdot ( 2 \cdot \lceil{log_{M-1}\frac{b_r}{M}}\rceil+ 1)$

---

## sort-merge externo - exemplo

![height:500px](./figs/classificacao_externa_1.svg)

---

## sort-merge externo - exemplo

![height:500px](./figs/classificacao_externa_2.svg)

---

## sort-merge externo - exemplo

![height:500px](./figs/classificacao_externa_3.svg)


---

## sort-merge externo - exemplo

![height:500px](./figs/classificacao_externa_4.svg)


---

## sort-merge externo - exemplo

![height:500px](./figs/classificacao_externa_5.svg)


---

## sort-merge externo - exemplo

![height:500px](./figs/classificacao_externa_6.svg)


---

## sort-merge externo - exemplo

![height:500px](./figs/classificacao_externa_7.svg)


---

## sort-merge externo - exemplo

![height:500px](./figs/classificacao_externa_8.svg)


---

## sort-merge externo - exemplo

![height:500px](./figs/classificacao_externa_9.svg)
