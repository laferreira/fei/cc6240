---
title: 03 - Otimização de consulta
author: Leonardo Anjoletto Ferreira
theme: custom
paginate: true
lang: pt-br
---

<!--
_header: CC6240 - Tópicos avançados em banco de dados
_footer: Leonardo Anjoletto Ferreira
-->

# Otimização de consulta

---

## Resumindo...

- usuário não escreve uma query pensando em eficiência...
- SGBD fica responsável pela elaboração da estratégia para recuperação eficiente dos dados
    * já vimos os algoritmos usados para consultas
    * agora veremos como as cláusulas são arranjadas

---

## Relembrando

- Propriedade associativa:
    - sendo a mesma operação, a ordem das operações não alteram o resultado
    - (a + b) + c = a + (b + c)

- Propriedade comutativa:
    - a ordem dos termos não afeta o resultado
    - a + b = c $\to$ b + a = c

---

## Mais termos...

- Junção Theta: produto cartesiano que gera uma combinação todos-para-todos
- Equi-junção: quando há condições de igualdade na junção

```sql
SELECT *
FROM r
INNER JOIN s ON r.id = s.id -- junção theta
WHERE r.valor = 100         -- equi-junção
```

---

![bg height:600px center](./figs/proc.svg)

---

## Árvore canônica

![bg left:40% 80%](./figs/arvore_canonica1.svg)

- começa com operações de nível mais baixo
- resultados armazenados em relações temporárias
- devemos considerar o custo das operações
- resultado de uma operação é passado para a próxima parte

---

## Árvore canônica

![bg left:40% 80%](./figs/arvore_canonica2.svg)

- começa com operações de nível mais baixo
- resultados armazenados em relações temporárias
- devemos considerar o custo das operações
- resultado de uma operação é passado para a próxima parte

---

## Árvore canônica

![bg left:40% 80%](./figs/arvore_canonica3.svg)

- começa com operações de nível mais baixo
- resultados armazenados em relações temporárias
- devemos considerar o custo das operações
- resultado de uma operação é passado para a próxima parte

---

## Árvore canônica

![bg left:40% 80%](./figs/arvore_canonica4.svg)

- começa com operações de nível mais baixo
- resultados armazenados em relações temporárias
- devemos considerar o custo das operações
- resultado de uma operação é passado para a próxima parte

---

## Quando há mais do que uma tabela envolvida

* Tabelas intermediárias são criadas para armazenar os dados necessários
* Pode ser que estas relações intermediárias sejam grandes...

---

## Otimização

dado uma expressão em álgebra relacional:
- o otimizador propõe um plano de avaliação que gera o mesmo resultado com menor custo
- gera planos alternativos de execução usando:
    - regras de equivalência algébrica: conhecidas pelo otimizador para gerar transformações válidas
    - algoritmos de otimização algébrica: indica a ordem de aplicação de regras e outros processos de otimização

---

# Regras de equivalência algébrica

---

## Regras de equivalência algébrica

- Expressões de duas formas diferentes que são equivalentes
- Preservar equivalência: relações geradas por expressões diferentes possuem o mesmo conjunto de atributos e de tuplas, apesar dos atributos estarem ordenados de forma diferente
- Notação:
    - Predicados: $\theta, \theta_1, \ldots, \theta_n$
    - Lista de atributos: $L_1, L_2, \ldots, L_n$
    - Relações: $E_1, E_2, \ldots, E_n$

---

## Otimização

1. Operações de seleção que usam AND (seleção conjuntiva) podem ser divididas em um sequência de seleções individuais
$$ S_{q_1 \cap q_2}(E) = S_{q_1}(S_{q_2}(E)) $$

---

## Otimização

2. Operações de seleção são comutativas
$$ S_{q_1}(S_{q_2}(E)) = S_{q_2}(S_{q_1}(E)) $$

<div class="columns">
<div>

```sql
WHERE x=1 AND y=2
```

</div>
<div>

```sql
WHERE y=2 AND x=1
```
</div>
</div>

---

## Otimização

3. Somente operações finais são necessárias em uma sequência de operações de projeção
$$ \Pi_{L_1}(\Pi_{L_2}(\Pi_{L_3}(\ldots(\Pi_{L_n}(E))))) = \Pi_{L_1}(E) $$

---

## Otimização

4. Se todos os atributos da condição forem atributos apenas de uma relação, seleções podem ser combinadas com os produtos cartesianos e junção theta
$$ \sigma_\theta (E_1 \times E_2) = E_1 \bowtie_\theta E_2 $$

---

## Otimização

5. Operações de junção theta e junção natural são comutativas

$$ (R \bowtie S) \bowtie T = R \bowtie (S \bowtie T) $$

* só precisamos tomar cuidado com o custo

---

## Otimização

6a. Operações de junção natural são associativas
$$ E_1 \bowtie E_2 = E_2 \bowtie E_1 $$
$$ (E_1 \bowtie_{\theta_1} E_2) \bowtie_{\theta_2} E_3 = E_1 \bowtie_{\theta_1} (E_2 \bowtie_{\theta_2} E_3) $$

6b. Junções theta são associativas seguindo:
$$ (E_1 \bowtie_{\theta_1} E_2) \bowtie_{\theta_2 \land \theta_3} E_3 = E_1 \bowtie_{\theta_1 \land \theta_3} (E_2 \bowtie_{\theta_2} E_3)$$

---

## Otimização

7. Operação de seleção pode ser distribuída por meio da operação de junção theta, nas condições:

- 7a. quando todos os atributos de $\theta_0$ envolvem somente atributos de uma das expressões da junção
$$ \sigma_{\theta_0}(E_1 \bowtie_\theta E_2) = (\sigma_{\theta_0}(E_1)) \bowtie_\theta E_2 $$


---

## Otimização

7. Operação de seleção pode ser distribuída por meio da operação de junção theta, nas condições:

- 7b. quando $\theta_1$ envolve somente atributos de $E_1$ e $\theta_2$ envolve somente atributos de $E_2$
$$ \sigma_{\theta_1 \land \theta_2} (E_1 \bowtie_\theta E_2) = (\sigma_{\theta_1}(E_1)) \bowtie_\theta (\sigma_{\theta_2}(E_2)) $$

---

## Otimização

8. A operação de projeção é distribuída pela operação de junção theta:
- Sendo $L_1$ e $L_2$ conjuntos de atributos de $E_1$ e $E_2$ respectivamente
- Se $\theta$ envolve unicamente atributos de $L_1 \cup L_2$
$$ \Pi_{L_1 \cup L_2}(E_1 \bowtie_\theta E_2) = (\Pi_{L_1}(E_1)) \bowtie_\theta (\Pi_{L_2}(E_2)) $$


---

## Otimização

9. Os conjuntos de operações de união e intersecção são comutativos

$$ E_1 \cup E_2 = E_2 \cup E_1 $$
$$ E_1 \cap E_2 = E_2 \cap E_1 $$

---

## Otimização

10. União e intersecção são associativas

$$ (E_1 \cup E_2) \cup E_3 = E_1 \cup (E_2 \cup E_3) $$
$$ (E_1 \cap E_2) \cap E_3 = E_1 \cap (E_2 \cap E_3) $$

---

## Otimização

11. Operação de seleção é distribuída sobre $\cup$, $\cap$ e $-$
$$ \sigma_\theta (E_1 - E_2) = \sigma_\theta (E_1) - \sigma_\theta(E_2) $$

---

## Otimização

12. Operação de projeção é distribuída por meio da operação de união
$$ \Pi_L(E_1 \cup E_2) = (\Pi_L(E_1)) \cup (\Pi_L(E_2)) $$

---

## Exemplo prático

- Para todas as relações $r1$, $r2$ e $r3$: $(r1 \bowtie r2) \bowtie r3 = r1 \bowtie (r2 \bowtie r3)$

- Se $r2 \bowtie r3$ for maior que $r1 \bowtie r2$, selecionamos $(r1 \bowtie r2) \bowtie r3$, usando a menor relação temporária possível

---

## Enumeração de expressões equivalentes

Otimizadores de consulta usam regras de equivalência para gerar sistematicamente expressões equivalentes para uma expressão de consulta

```
Repita até que nenhuma nova expressão equivalente possa ser gerada:

    1. Aplicar todas as regras de equivalência possíveis sobre
       toda expressão encontrada até o momento

    2. Adicionar expressões geradas ao conjunto de
       expressões equivalentes
```
* Porém esta abordagem tem custo de tempo e espaço muito alto

---

## Enumeração de expressões equivalentes

Outras abordagens:
- Geração do plano otimizado baseado nas regras de transformação
- Abordagem especial para consultas com somente seleções, projeções e junções

---

## Escolha do plano de avaliação

- Em função das estatísticas das relações de entrada
- Algoritmo mais barato para cada operação
- Problema: algoritmos mais barato para cada operação pode não ser o melhor considerando o plano total
    * junção mais custosa mas que fornece saída ordenada que reduz o custo da próxima operação
    * junções que podem fornecer facilidades para o processamento da query

---

## Escolha do plano de avaliação - otimização

Otimizadores práticos:
1. Buscam todos os planos e escolhem o melhor a partir do custo
2. Usa heurística para escolher um plano

---

## 1. Otimização baseada em custo

<div class="columns">
<div>

- Usa regras de equivalência para gerar faixa de planos de avaliação a partir de uma determinada consulta
- Escolhe o plano de menor custo
- Problemas:
    - Quantidade de planos
    - Quantidade de ordens de junção

</div>
<div>

Considerando $r_1 \bowtie r_2 \bowtie \ldots \bowtie r_n$:
- $n=5 \to 1.680$ ordens de junção diferentes
- $n=7 \to 665.280$ ordens de junção diferentes
- $n=10 \to 17.6$ bilhões ordens de junção diferentes
$$ \frac{(2(n-1))!}{(n-1)!} $$

</div>
</div>

---

## 2. Programação dinâmica

- Muito usado em problemas de otimização que envolvem combinações
- Aplicável em problemas que a solução ótima pode ser calculada partir da solução ótima de subproblemas, evitando recálculo

---

## 2. Programação dinâmica

- Exemplo: $(r1 \bowtie r2 \bowtie r3) \bowtie r4 \bowtie r5$
    - 12 ordens de junção para $(r1 \bowtie r2 \bowtie r3)$
    - Sabendo a melhor ordem para a parte anterios, usar o resultado para calculas $\bowtie r4 \bowtie r5$
    - Portanto, $12 + 12$ comparações no lugar de $12^{12}$

---

## 2. Programação dinâmica

- O tempo de otimização usando programação dinâmica com árvores é de $O(3^n)$

- Considerando $r_1 \bowtie r_2 \bowtie \ldots \bowtie r_n$, com $n = 10$, temos 59.000 comparações
* no método anterior temos 17,6 bilhões...

---

## 3. Otimização heurística

- Existem casos que calcular o custo é caro
- Proposta: usar heurísticas para transformar consultas, independente do custo
- Exemplos:
    - Executar operações de seleção assim que possível
    - Executar projeções antes de outras operações

---

## 3. Otimização heurística

- Decompor as seleções conjuntivas em uma sequência de operações de seleção simples (regra de equivalência 1)
- Mover as operações de seleção para a parte inferior da árvore de consultas para poder executá-las o mais cedo possível (regras de equivalência 2, 7a, 7b e 10)
- Executar primeiro as operações de seleção e junção que produzirão as relações de menor tamanho (regra de equivalência 6 e 10)

---

## 3. Otimização heurística

- Trocar as operações de produto cartesiano seguidas por uma condição de seleção por operações de junção (regra de equivalência 4)
- Decompor e mover para baixo da árvore o quanto possível as listas de atributos de projeção, criando novas projeções onde seja necessário (regras de equivalência 3, 8a e 11)

---

## SGBDs e otimização

- Maioria dos otimizadores dos SGBDs combinam elementos de custo com elementos de heurísticas
- Mesmo com o uso de heurísticas, a otimização baseada no custo da consulta impõe um overhead considerável
- Este custo é mais do que compensado pela economia no tempo de execução da consulta, especificamente pela redução do número de acessos de discos lentos
