---
title: 06 - Transações
author: Leonardo Anjoletto Ferreira
theme: custom
paginate: true
lang: pt-br
---

<!--
_header: CC6240 - Tópicos avançados em banco de dados
_footer: Leonardo Anjoletto Ferreira
-->

# Transações

---

## Transações

- Definição: unidade de execução do programa que acessa ou atualiza vários itens de dados (i.e., uma sequência de leituras e gravações)
- Em geral é iniciada por um programa do usuário e escrita em liguágem de alto nível
- Deve manter as propriedades ACID

---

## ACID

- **Atomicidade**: todas operações são executadas ou nenhuma delas;
- **Consistência**: transações devem manter consistência nos dados (sem transações simultâneas no mesmo dado);
- **Isolamento**: uma transação não tem conhecimento da outra;
- **Durabilidade**: após transação, as mudanças persistem no banco mesmo se houver falhas no sistema;

---

## Exemplo

<div class="columns">
<div>

Considerando apenas 2 operações:
- `read(x)`: transfere dados do banco para o buffer
- `write(x)`: transfere dados do buffer para o banco

</div>
<div>

```
read(A);
A := A - 50;
write(A);

read(B);
B := B + 50;
write(B);
```

</div>
</div>

---

## Exemplo: Consistência

<div class="columns">
<div>

- A + B permanece inalterado
- garantia de consistência de uma transação individual é dever do programador

</div>
<div>

```
read(A);
A := A - 50;
write(A);

read(B);
B := B + 50;
write(B);
```

</div>
</div>

---

## Exemplo: Atomicidade

<div class="columns">
<div>

- ou faz as duas operações ou desfaz tudo o que foi feito
- garantido pelo componente de gerenciamento de transação e recuperação do SGDB

</div>
<div>

```
read(A);
A := A - 50;
write(A);

read(B);
B := B + 50;
write(B);
```

</div>
</div>

---

## Exemplo: Durabilidade

<div class="columns">
<div>

- após transação ser executada, valores persistem no banco mesmo se houver falha no sistema
- garantido pelo componente de gerenciamento de recuperação do SGDB

</div>
<div>

```
read(A);
A := A - 50;
write(A);

read(B);
B := B + 50;
write(B);
```

</div>
</div>

---

## Exemplo: Isolamento

<div class="columns">
<div>

- outras transações em execução no mesmo momento não afetam a transação atual
- garantido pelo componente controle de concorrência do SGDB

</div>
<div>

```
read(A);
A := A - 50;
write(A);

read(B);
B := B + 50;
write(B);
```

</div>
</div>

---

## Estados de transação
- Estados possíveis da transação:
    - ativa: enquanto está executando
    - parcialmente confirmada: após execução da instrução final
    - falha: quando descobre-se que execução normal não pode prosseguir
    - abortada: depois que transação foi revertida e o banco restaurado ao estado anterior ao início da transação
    - confirmada: após o término bem sucedido

---
## Estados de transação

<div class="columns">
<div>

- Transação completada com sucesso $\to$ transação confirmada (commited)
- Em caso de falhas: transação abortada $\to$ transação revertida (rolled back)
* Após confirmada, só pode ser revertida pelo usuário/aplicação

</div>
<div>

![center](./figs/diagrama_estados_transicao.svg)
</div>
</div>

---

## Estados de transação

- Uma transação pode ser reiniciada se tiver sido abortada por algum erro que não seja de lógica (hardware ou software)
- O SGDB pode matar uma transação devido a erro lógico interno que só pode ser corrigido com reescrita de programa
- Para garantir atomicidade, a maioria dos SGDB não permitem escritas externas observáveis em transações. Em geral, estas operações são armazenadas em memória volátil e executadas depois

---

## Execução serial de transações

Abordagem mais simples:
1. Executar transação
2. SGDB executa validação
3. Transação concluída

---

## Schedule

- Schedule: define a ordem de execução das operações de várias transações
- Considerando as transações como exemplo:

<div class="columns">
<div>
T1:

```
read(A);
A := A - 50;
write(A);

read(B);
B := B + 50;
write(B);
```

</div>
<div>
T2:

```
read(A);
temp := A * 0.1;
A := A - temp;
write(A);

read(B);
B := B + temp;
write(B);
```

</div>
</div>

---

## Schedule serial
- Schedule serial:
    - ordem cronológica de executar transações
    - precisa consistir todas as instruções da transação
    - precisa preservar a ordem das instruções em cada transação (uma transação só começa após a outra terminar)
    - não executa transações simultâneas
- No exemplo, podemos ter:
    - primeiro T1 e depois T2
    - primeiro T2 e depois T1
---

## Execução simultânea de transações

- Melhor _throughput_ (quantidade de transações por quantidade de tempo)
- Melhor utilização dos recursos
- Menor tempo de espera

- Problema: consistência

---

## Execução simultânea de transações

- Se SGDB aceita realizar:
    - Schedule não precisa ser serial
    - SO pode gerenciar a execução das transações
    - Pode ocorrer o compartilhamento de recursos
- Várias sequências de execução possíveis:
    - Intercalação de instruções
    - Mais variações do que o schedule sequencial

---

## Execução simultânea de transações


<div class="tables">

| T1           | T2               |
|--------------|------------------|
| read(A);     |                  |
| A := A - 50; |                  |
| write(A);    |                  |
|              | read(A);         |
|              | temp := A * 0.1; |
|              | A := A - temp;   |
|              | write(A);        |
| read(B);     |                  |
| B := B + 50; |                  |
| write(B);    |                  |
|              | read(B);         |
|              | B := B + temp;   |
|              | write(B);        |

</div>

---

## Execução simultânea de transações

- Problema: obter um schedule que contenha execuções simultâneas equivalente a um schedule serial
- Para garantir que os schedules não darão errado, o controle fica com o SGDB e não com o SO (garantia de consistência)
- No SGDB, o componente de controle de concorrência é responsável por esta tarefa
- Estratégias de verificação de equivalência:
    - Serialização por conflito
    - Serialização por visão


---

## Execução simultânea de transações

Interessam somente as instrução `read` e `write` para efeitos de escalonamento:


<div class="tables">

| T1        | T2        |
|-----------|-----------|
| read(A);  |           |
| write(A); |           |
|           | read(A);  |
|           | write(A); |
| read(B);  |           |
| write(B); |           |
|           | read(B);  |
|           | write(B); |

</div>

---

## Serialização de conflito

Schedule $S$ com duas instruções consecutivas $I_i$ e $I_j$ pertencentes às transações $T_i$ e $T_j$ respectivamente:
- Se $I_i$ e $I_j$ se referem a diferentes dados, a inversão da ordem de execução de $I_i$ e $I_j$ não afeta o resultado das instruções do schedule
- Se $I_i$ e $I_j$ se referem ao mesmo dado, a ordem pode alterar o resultado:
    - Se forem duas operações de `read`, a ordem não importa
    - Se pelo menos uma for operação de `write`, existe conflito

---

## Serialização de conflito

Considerando $I_i$ e $I_j$ como instruções consecutivas em um schedule:
- Se forem instruções de transações diferentes e não conflitarem, podemos inverter a ordem, gerando um novo schedule equivalente
- Invertendo as instruções que forem possíveis, obtemos um schedule equivalente ao schedule serial
- Se for possível transformar um schedule $S$ em um schedule $S'$ por uma série de trocas de instruções não conflitantes: $S$ e $S'$ são equivalentes em conflito


---

## Serialização de conflito: exemplo

<div class="tables">

| T1        | T2        |
|-----------|-----------|
| read(A);  |           |
| write(A); |           |
|           | read(A);  |
|           | write(A); |
| read(B);  |           |
| write(B); |           |
|           | read(B);  |
|           | write(B); |

</div>

---

## Serialização de conflito: exemplo

<div class="tables">

| T1        | T2        |
|-----------|-----------|
| read(A);  |           |
| write(A); |           |
|           | read(A);  |
| read(B);  |           |
|           | write(A); |
| write(B); |           |
|           | read(B);  |
|           | write(B); |

</div>

---

## Serialização de conflito: exemplo

<div class="tables">

| T1        | T2        |
|-----------|-----------|
| read(A);  |           |
| write(A); |           |
| read(B);  |           |
|           | read(A);  |
|           | write(A); |
| write(B); |           |
|           | read(B);  |
|           | write(B); |

</div>

---

## Serialização de conflito: exemplo

<div class="tables">

| T1        | T2        |
|-----------|-----------|
| read(A);  |           |
| write(A); |           |
| read(B);  |           |
|           | read(A);  |
| write(B); |           |
|           | write(A); |
|           | read(B);  |
|           | write(B); |

</div>

---

## Serialização de conflito: exemplo

<div class="tables">

| T1        | T2        |
|-----------|-----------|
| read(A);  |           |
| write(A); |           |
| read(B);  |           |
| write(B); |           |
|           | read(A);  |
|           | write(A); |
|           | read(B);  |
|           | write(B); |

</div>

---

## Serialização de visão
- Menos rigorosa do que por conflito
- Considerada serialização de visão se for equivalente à algum schedule serial

---

## Serialização de visão

Dois schedules $S$ e $S'$ são equivalentes se:
1. Schedule inicial e final tem `read(Q)` correspondente: para cada item de dado $Q$, se $T_i$ ler o valor de $Q$ no schedule $S$, $T_i$ em $S'$ também precisa ler o valor inicial de $Q$

---

## Serialização de visão

Dois schedules $S$ e $S'$ são equivalentes se:
2. Para cada item de dado $Q$ produzido por uma operação `write(Q)` por $T_j$, se $T_i$ executar `read(Q)` no schedule $S$ então em $S'$ a operação `read(Q)` em $T_i$ deve ler o mesmo valor produzido por `write(Q)` em $T_j$

---

## Serialização de visão

Dois schedules $S$ e $S'$ são equivalentes se:
3. Para cada item de dado $Q$, a transação que realiza `write(Q)` ao final de $S$ precisa realizar a mesma operação ao final de $S'$

---

## Verificação de serialização

- Esquemas de controle de concorrência geram schedules que devem ser serializados
- Grafo de precedência G = (V,E), em que:
    - vértices: todas as transações participantes do schedule
    - arestas: $T_i \to T_j$ se uma das condições for verdadeira:
        - $T_i$ executa `write(Q)` antes que $T_j$ execute `read(Q)`
        - $T_i$ executa `read(Q)` antes que $T_j$ execute `write(Q)`
        - $T_i$ executa `write(Q)` antes que $T_j$ execute `write(Q)`
    - se $T_i \to T_j$ existir no gráfico de procedência em um schedule $S$, ele deve existir em qualquer schedule equivalente $S'$

---

## Verificação de serialização

<div class="columns">

<div>
<div class="tables">

| T1           | T2               |
|--------------|------------------|
| read(A);     |                  |
| A := A - 50; |                  |
| write(A);    |                  |
| read(B);     |                  |
| B := B + 50; |                  |
| write(B);    |                  |
|              | read(A);         |
|              | temp := A * 0.1; |
|              | A := A - temp;   |
|              | write(A);        |
|              | read(B);         |
|              | B := B + temp;   |
|              | write(B);        |

</div>
</div>
<div>

Grafo de precedência

![](./figs/grafo_prec_1.svg)

</div>
</div>

---

## Verificação de serialização

<div class="columns">
<div>
<div class="tables">

| T1           | T2               |
|--------------|------------------|
|              | read(A);         |
|              | temp := A * 0.1; |
|              | A := A - temp;   |
|              | write(A);        |
|              | read(B);         |
|              | B := B + temp;   |
|              | write(B);        |
| read(A);     |                  |
| A := A - 50; |                  |
| write(A);    |                  |
| read(B);     |                  |
| B := B + 50; |                  |
| write(B);    |                  |

</div>
</div>
<div>

Grafo de precedência

![](./figs/grafo_prec_2.svg)

</div>
</div>

---

## Verificação de serialização

<div class="columns">
<div>
<div class="tables">

| T1           | T2               |
|--------------|------------------|
| read(A);     |                  |
| A := A - 50; |                  |
|              | read(A);         |
|              | temp := A * 0.1; |
|              | A := A - temp;   |
|              | write(A);        |
|              | read(B);         |
| write(A);    |                  |
| read(B);     |                  |
| B := B + 50; |                  |
| write(B);    |                  |
|              | B := B + temp;   |
|              | write(B);        |

</div>
</div>
<div>

Grafo de precedência

![](./figs/grafo_prec_3.svg)

</div>
</div>

---

## Verificação da serialização

- Ordem da serialização:
    - classificação topológica do grafo de precedência
    - ordem linear consistente com a ordem parcial do gráfico de precedência
    - normalmente exitem várias ordens de serialização possiveis
- Testar serialização por conflito: construir grafo de precedência e executar algoritmos de detecção de ciclos
- Testar serialização de visão: ainda não existe algoritmo eficiente. Verifica-se apenas as condições suficientes



---

## Verificação da serialização: exemplo

<div class="columns">
<div>

![](./figs/grafo_prec_4.svg)

</div>
<div>

Possíveis ordens de serialização:

- $T_1 \to T_2 \to T_3 \to T_4$
- $T_1 \to T_3 \to T_2 \to T_4$

</div>
</div>
