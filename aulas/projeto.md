# Projeto: Polyglot Persistence

O objetivo deste projeto é estudar o armazenamento de dados em diversos bancos tendo o uso do dado pela aplicação como critério de escolha para o tipo de banco que será usado.

O tema do projeto é livre, porém a implementação **deve** seguir o seguinte modelo:

```
                                          -------
                                --------> |     |
                                |         | DB1 |
                                | ------- |     |
                                | |       -------
                                | v
------      --------------     ------     -------
|    |      |            | --> |    | --> |     |
| S1 | ---> | mensageria |     | S2 |     | DB2 |
|    |      |            | <-- |    | <-- |     |
------      --------------     ------     -------
  |            |                 ^ |
  |   ------   |                 | |      -------
  |   |    |   |                 | -----> |     |
  --->| S3 |<--|                 |        | RDB |
      |    |                     ---------|     |
      ------                              -------
```
em que:
- RDB é um banco relacional;
- DB1 e DB2 são bancos que não podem ser relacionais (NoSQL);
- mensageria é um serviço de mensagens que pode ser escolhido entre existentes (e.g., Apache Kafka, RabbitMQ, Apache ActiveMQ, etc) ou implementado pelo desenvolvedor desde que siga o padrão pub-sub;
- S1: serviço que deve gerar mensagens que serão enviadas ao sistema de mensageria. Estas mensagens podem ser tanto para gerar dados fictícios que serão armazenados no bancos como requisições de dados que estão nos bancos e que devem ser retornadas. Os dados fictícios gerados devem ser de pelo menos 3 tipos diferentes, sendo que cada um deles será armazenado em um banco diferente;
- S2: serviço(s) que receberão as mensagens que estão no sistema de mensageria e devem realizar o armazenamento ou busca dos dados nos respectivos bancos. Esta parte pode ser implementada como apenas 1 serviço que lê todos os tipos de mensagem ou como diversos serviços que são responsáveis por tipos de mensagens diferentes;
- S3: serviço deve armazenar tanto as mensagens enviadas por S1 como as retornadas pela mensageria. Este serviço será usado para validar que todas as mensagens enviadas pelo serviço tiveram uma resposta e que está está correta e pode usar arquivos ou um banco de dados para armazenar as mensagens.

## Exemplo de tema

Um marketplace em que:
- RDB: armazena dados dos clientes cadastrados;
- DB1: um banco do tipo document storage para armazenar os dados dos produtos;
- DB2: um wide-column storage para armazenar os dados de pedidos realizados;
- S1: deve gerar mensagens sobre CRUD de clientes, CRUD de produtos e compra de produtos por clientes;
- S2: dividido em 3 serviços com um deles responsável pela parte do CRUD do cliente, um para o CRUD de produtos e o último para tratar os pedidos de compra
- S3: lê as mensagens enviadas por S1 e enviadas para S1 e armazena no Elastic Search

## Entrega

A entrega do projeto será por meio de um repositório de um sistema de versionamento (i.e., github, gitlab, bitbucket, etc) que deve conter:
- todo o código desenvolvido para o projeto
- README.md com:
    1. a explicação do tema escolhido;
    2. justificativa para cada banco usado no projeto e definição de como S2 será implementado;
    3. explicação de como executar o projeto e quais serviços devem ser usados;
- todos os recursos necessários para executar o projeto considerando um ambiente sem nenhuma execução prévia do projeto;

## Desenvolvimento do projeto

O projeto será dividido em 2 partes sendo a primeira com foco na especificação do projeto e a segunda na implementação.

Desta forma, na primeira entrega deverá ser apresentado no `README.md` as explicações para os itens 1 e 2 do tópico Entrega. A segunda entrega deverá conter os itens da primeira entrega além de todos os outros listados.
