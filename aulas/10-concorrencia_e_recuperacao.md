---
title: 07.1 - Concorrência
author: Leonardo Anjoletto Ferreira
theme: custom
paginate: true
lang: pt-br
---

<!--
_header: CC6240 - Tópicos avançados em banco de dados
_footer: Leonardo Anjoletto Ferreira
-->

# Concorrência e recuperação

---

## Concorrência

Aula passada: serialização de transações para executar as instruções de mais do que uma transação

Nesta aula:
    - Formas de execução de instruções em paralelo, usando técnicas de bloqueio de dados
    - Formas de Recuperação


---

## Bloqueio binário

- Impedir que múltiplas transações acessem os dados ao mesmo tempo
- Se uma transação precisa de um item bloqueado, então deve esperar até que este item seja liberado
- Operações:
    - `LOCK(X)`/`UNLOCK(X)`: bloqueia/desbloqueia o item `X`
    - Item pode ter dois estados: _locked_ (1) e _unlocked_ (0)

- O bloqueio é por item do banco
---

<div class="columns" >
<div>

- `LOCK(X)`: bloqueia o item `X` fazendo com que o valor de bloqueio seja 1, não permitindo que outra operação acesse (X)

- `UNLOCK(X)`: desbloqueia o item `X` (valor de bloqueio igual a 0) e permite o acesso por outra operação


</div>
<div>

| T1         | T2          |
|------------|-------------|
| lock(x)    | lock(x)     |
| read(x)    | read(X)     |
| x = x - 50 | x = x + 100 |
| write(A)   | write(A)    |
| unlock(X)  | unlock(X)   |
| lock(Y)    |             |
| read(Y)    |             |
| Y = Y + 50 |             |
| write(Y)   |             |
| unlock(Y)  |             |

</div>
</div>

---

## Bloqueio compartilhado

- Bloqueio compartilhado:
    - `lock-s(X)` (`s` de `s`hared)
    - diversas transações podem acessar `X` se todas forem `read`
- Bloqueio exclusivo:
    - `lock-x(X)` (`x` de e`x`clusive)
    - se a transação fizer `write`, apenas esta transação pode acessar `X`
- Desbloqueio: `unlock(X)`

---

## Bloqueio compartilhado

<div class="columns">
<div>

<div class="tables">

| T1         | T2          |
|------------|-------------|
| lock-x(X)  | lock-x(X)   |
| read(X)    | read(X)     |
| X = X - 50 | X = X + 100 |
| write(X)   | write(A)    |
| unlock(X)  | unlock(X)   |
| lock-x(Y)  |             |
| read(Y)    |             |
| Y = Y + 50 |             |
| write(Y)   |             |
| unlock(Y)  |             |

</div>
</div>

<div>

* Mudança na execução se trocar `lock-x` por `lock-s`
* Uso de concorrência: usar `lock-x` para escrita e `lock-s` para leitura

</div>

---

## Bloqueios, deadlocks e estagnação

- Estagnação: se majoritariamente ocorrer leitura nas transações, uma transação pode esperar muito tempo para ter acesso aos dados
- Deadlock: uma transação $T_i$ não termina pois espera a liberação dos dados por $T_j$, mas $T_j$ não termina pois espera a liberação dos dados por $T_i$ (podem ser encontrados por ciclos em grafos de precedência)
- Existem protocolos adicionais para o posicionamento de operações de bloqueio e desbloqueio

---

## Two Phase Commit (2PC)

Two Phase Commit ou Duas fases de bloqueio: todas as operações de bloqueio (`lock-x` e `lock-s`) precedem a primeira operação de desbloqueio (`unlock`).

Pelo nome, consiste em duas fases:
1. Fase de crescimento: transação pode bloquear mas não pode liberar qualquer bloqueio
2. Fase de encolhimento: transação pode liberar bloqueios, mas não pode obter novos bloqueios

---

<div class="columns">
<div>

### Exemplo de transação de uma fase

- `lock`s e `unlock`s misturados
- para as duas fases, vamos separar `lock` e `unlock`

</div>

<div>
<div class="tables">

| T1         | T2           |
|------------|--------------|
| lock-x(B)  |              |
| read(B)    |              |
| B = B - 50 |              |
| write(B)   |              |
| unlock(B)  |              |
|            | lock-s(A)    |
|            | read(A)      |
|            | unlock(A)    |
|            | lock-s(B)    |
|            | read(B)      |
|            | unlock(B)    |
| lock-x(A)  |              |
| read(A)    |              |
| A = A + 50 |              |
| write(A)   |              |
| unlock(A)  |              |

</div>
</div>

---
## Exemplo de transação de duas fases

<style scoped>
div {
  font-size: 10px;
}
</style>
<div class="tables">

| fase de T1       | T1         | T2           | fase de T2       |
|------------------|------------|--------------|------------------|
| **crescimento**  | lock-x(B)  |              |                  |
|                  | read(B)    |              |                  |
|                  | B = B - 50 |              |                  |
|                  | write(B)   |              |                  |
|                  |            |              |                  |
|                  |            | lock-s(A)    | **crescimento**  |
|                  |            | read(A)      |                  |
|                  |            | lock-s(B)    |                  |
|                  |            |              |                  |
|                  |            | read(B)      | **encolhimento** |
|                  |            | display(A+B) |                  |
|                  |            | unlock(A)    |                  |
|                  |            | unlock(B)    |                  |
|                  | lock-x(A)  |              |                  |
|                  |            |              |                  |
| **encolhimento** | read(A)    |              |                  |
|                  | A = A + 50 |              |                  |
|                  | write(A)   |              |                  |
|                  | unlock(B)  |              |                  |
|                  | unlock(A)  |              |                  |

</div>

---

## Granularidade

- A escolha da granularidade pode afetar a execução do controle de concorrência
- O conjunto de itens de dados podem ser:
    - Todo o banco
    - Arquivo
    - Bloco do disco
    - Registro
    - Campo
- Quanto maior o tamanho do item de dado, mais baixo o grau de concorrência permitido

---

## Granularidade

![h:500px](./figs/granularidade.svg)

---

## Granularidade

- Uma transação $T_1$ atualiza todos os registros de um arquivo $A_1$ e recebe o bloqueio exclusivo deste arquivo. Como o bloqueio é em 1 arquivo, ele é mais eficiente do que em vários blocos ou registros
* Uma transação $T_2$ quer acessar um registro $ra_2$ na página $f_a$ de $A_1$: o gerenciador de bloqueios deve verificar a compatibilidade do bloqueio com os que já estão feitos.

---

## Granularidade

- Se $T_2$ ocorrer depois de $T_1$: se houver bloqueio em conflito, o bloqueio de $ra_2$ é negada para $T_2$
- Se $T_2$ ocorrer antes de $T_1$: o bloqueio de $ra_2$ ocorre e o gerenciador deve verificar o bloqueio para $A_1$, gerando bloqueio em múltiplos níveis de granularidade sendo menos eficiente que a forma anterior

---

## Bloqueios por intenção

- Tipos de bloqueio usados para granularidade múltipla
- A transação deve indicar ao logo do caminho até o dado qual tipo de bloqueio (compartilhado ou exclusivo) requer

---

## Bloqueios por intenção

Além de bloqueio compartilhado (`s`) e exclusivo (`x`), temos:
1. compartilhado por intenção (`is`): indica que um ou mais bloqueios compartilhados serão solicitados em algum ou alguns nós descendentes
2. exclusivo por intenção (`ix`): um ou mais bloqueios exclusivos serão solicitados em algum ou alguns nós descendentes
3. compartilhado-exclusivo por intenção (`six`): nó atual está bloqueado como compartilhado mas um ou mais blocos exclusivos serão solicitados

---

## Matriz de compatibilidade

tipo | IX  | S   | SIX | X
-----|-----|-----|-----|-----
 IS  | sim | sim | sim | sim
 IX  | sim | sim | não | não
 S   | sim | não | sim | não
 SIX | sim | não | não | não
 X   | não | não | não | não

---

## Granularidade - protocolo - parte 1

- Compatibilidade de bloqueio deve ser respeitada
- A raiz da árvore deve ser bloqueada primeiro em qualquer modo
- Um nó $N$ pode ser bloqueado por uma transação $T$ no modo $S$ ou $IS$ somente se o nó pai de $N$ já estiver bloqueado por $T$ no modo $IS$ ou $IX$
- Um nó $N$ pode ser bloqueado por uma transação $T$ no modo $X$, $IX$ ou $SIX$ somente se o nó pai de $N$ já estiver bloqueado no modo $IX$ ou $SIX$

---

## Granularidade - protocolo - parte 2

- Uma transação $T$ pode bloquear um nó somente se não tiver desbloqueado nenhum nó (bloqueio em duas fases)
- Uma transação $T$ pode desbloquear um nó $N$ somente se nenhum dos filhos de $N$ estiver atualmente bloqueado por $T$

---

## Granularidade - exemplo

- T1 deseja atualizar os registros rb1 e rc3
- T2 deseja atualizar todos os registros na página Fb
- T3 deseja ler o registro ra1 e o arquivo A2

<div class="tables">

| T1     | T2     | T3     |
|--------|--------|--------|
| IX(db) | IX(db) | IS(db) |
| IX(A1) | IX(A1) | IS(A1) |
| IX(Fa) | X(Fb)  | IS(Fa) |
| X(ra1) |        | S(ra1) |
| IX(Fc) |        | S(A2)  |
| X(rc3) |        |        |

</div>

---

## Cópia de sombra

- considera apenas uma transação ativa e banco como um arquivo em disco
- para atualizar o banco, é feita uma cópia de todo o banco e a atualização é feita sobre ele
- em caso de transação abortada: cópia de sombra é excluída
- em caso de transição concluída:
    - SO confirma se tudo foi gravado em disco
    - ponteiro do banco passa a apontar para a nova cópia
    - cópia antiga é excluída

---

## Cópia de sombra

- Tratamento de falhas:
    - na transação: exclui nova cópia do banco
    - no sistema:
        - se ponteiro não foi atualizado, ainda aponta para a cópia antiga
        - se ponteiro foi atualizado, banco consegue ver todas as atualizações feitas pois os dados foram gravados em disco

---

## Cópia de sombra

- Vantagens: esquema simples de atomicidade e durabilidade
- Desvantagens:
    - cópia de todo o banco
    - não permite transações simultâneas

---

## Facilidade de recuperação

- Necessidade de desfazer a transação para garantir a atomicidade, se houver falha em uma transação executada de forma simultânea

- Schedules aceitáveis para recuperação:
    - Em cascata e não em cascada
    - schedules estritos

---

## Schedules recuperáveis

- Garantia que se uma transação foi confirmada (commit), ela será abortada
- Um schedule $S$ é recuperável se nenhuma transação $T_i$ em $S$ for concluída até que todas as transações que gravaram dados lidos por $T_i$ tenham sido concluídas
- Para que o banco se recupere corretamente de uma transação $T_i$ falha, é necessário reverter todas as transações que leram dados escritos por $T_i$

---

## Schedules recuperáveis

<div class="columns">
<div>

Considerando que T2 foi confirmado logo após a execução de `read(A)` e antes de T1 ser confirmada:

* Se T1 falhar antes de ser confirmada: T2 leu o valor de A escrito por T1 e deveria ser abortada para garantir atomicidade
* Mas como T2 foi confirmada, não pode ser abortada

</div>
<div>

* Schedule não recuperável que não pode ser permitido

<div class="tables">

| T1       | T2      |
|----------|---------|
| read(A)  |         |
| write(A) |         |
|          | read(B) |
| read(B)  |         |

</div>

---

## Schedules em cascata e não em cascata

Considere 3 transações $T_1$, $T_2$ e $T_3$ executadas na ordem $T_1 \to T_2 \to T_3$:
- Se $T_1$ falhar após a execução de $T_3$, então é necessário fazer o rollback das 3 transações
- Neste caso, será feito um rollback em cascata
* Desperdício de trabalho pois teremos que fazer rollback em 3 transações

---

## Schedules em cascata e não em cascata

Preferencia por schedules não em cascata:
- Simplesmente porque o rollback em cascata não pode acontecer
- Sendo:
    - duas transações $T_i$ e $T_j$
    - $T_j$ lê item de dados previamente escrito por $T_i$
    - a operação de commit de $T_i$ deve aparecer antes da operação de read de $T_j$

---

## Escalonamento recuperável com aborto em cascata

<div class="tables">

| T1         | T2         |
|------------|------------|
| read(X)    |            |
| X = X - 20 |            |
| write(X)   |            |
|            | read(X)    |
|            | X = X + 10 |
|            | write(X)   |
| abort()    |            |

</div>

---

## Escalonamento recuperável sem aborto em cascata

<div class="tables">

| T1         | T2         |
|------------|------------|
| read(X)    |            |
| X = X - 20 |            |
| write(X)   |            |
| commit()   |            |
|            | read(X)    |
|            | X = X + 10 |
|            | write(X)   |

</div>

---

## Schedules estritos

- Schedule $S$ é recuperável, não cascata e estrito se $T_i$ só puder ler ou atualizar um dado depois que todas as outras transações que atualizam o mesmo dado forem concluídas

- Garante que se a transação tiver que ser revertida, só é necessário gravar antes as imagens dos dados atualizados pela transação

---

### Schedule recuperável sem aborto em cascata e não-estritos

<div class="tables">

| T1         | T2         |
|------------|------------|
| read(X)    |            |
| X = X - 20 |            |
| write(X)   |            |
|            | read(Y)    |
|            | X = Y + 10 |
|            | write(X)   |
|            | commit()   |
| abort()    |            |

</div>

---

### Schedule recuperável sem aborto em cascata e estritos

<div class="tables">

| T1         | T2         |
|------------|------------|
| read(X)    |            |
| X = X - 20 |            |
| write(X)   |            |
| commit()   |            |
|            | read(Y)    |
|            | X = Y + 10 |
|            | write(X)   |
|            | commit()   |

</div>

---

### Recuperação

- Não em cascata:
    - evita leitura de dado não atualizado
    - $T_i$ atualiza um dados e outras transações leem posteriormente

- Estrito:
    - evita atualização tardia
    - $T_i$ pode sobrescrever um dado escrito por uma outra transação

---

## Recuperação

- O sistema deve manter informações (logs) sobre os dados alterados pelas transações
- Estratégias para recuperação:
    - Dano extenso ou falha catastrófica: restaurar cópia anterior, reconstruir o estado mais atual e reaplicar/refazer operações contidas no log
    - Banco inconsistente por conta de falha não catastrófica: desfazer ou refazer operações de forma a restaurar um estado consistente

---

## Recuperação

- Para garantir a coerência e atomicidade em caso de falha:
    - Ações tomadas **durante** o processamento da transação para obter informações que podem ser usadas para recuperar das falhas
    - Ações tomadas **após** a falha para recuperar o conteúdo do banco a um estado consistente, atômico e durável

- Em geral, a recuperação está fortemente ligada às funções do sistema operacional

---

## Recuperação

- Log com a sequência de registros contendo todas as atualizações do banco é a estrutura normalmente usada para registrar modificações

- Técnicas de recuperação de falhas não catastróficas usando log:
    - Modificação adiada
    - Modificação imediata

---

## Recuperação por modificação adiada

Posterga qualquer atualização real no banco
- Durante a execução da transação, as transações não são executadas no banco e somente registradas no log
- Se o sistema falhar antes que a transação termine, as informações no log são ignoradas
- Somente o novo valor é registrado no log

---

## Recuperação por modificação imediata

- Atualizações são enviadas ao banco enquanto a transação está ativa
- Dois procedimentos de recuperação:
    - `undo(Ti)`: restaura o valor antigo (anterior a execução de Ti)
    - `redo(Ti)`: define o valor novo (após execução de Ti)

---

## Recuperação por modificação imediata

- O sistema precisa do valor antigo para restaurar dados modificados:
    - Operação de `undo` realiza a restauração
    - Log atualizado antes da atualização no banco
    - Commit precisa esta desabilitado
- Também conhecido como `undo`/`redo`

---

## Recuperação por modificação imediata

Para determinar quais operações precisam ser refeitas e desfeitas, usamos o log:
1. $T_i$ deve ser **desfeita** se log contém `<begin>` mas não contém `<commit>`
2. $T_i$ deve ser **refeita** se log contém `<begin>` e `<commit>`

---

## Checkpoints

- Consultar o log pode ser um processo demorado
- Em geral as transações já enviam as informações ao banco
- Checkpoints são periodicamente realizados pelo sistema para reduzir a sobrecraga
- Sistema de recuperação do SGDB é o responsável pelo intervalo de checkpoints

---

## Checkpoints

- Considerando:
    - Transações $T_0, \ldots T_n$:
    - Checkpoint mais recente em $T_i$ com $0 < i < n$
    - Somente necessário verificar as transações entre $T_i$ e $T_n$ para realizar a recuperação

---

## Checkpoints e commits

- Checkpoint:
    - controlado pelo SGDB ou administrador
    - transação não finalizada e dados gravados em disco e
- Commit:
    - controlado pelo usuário
    - transação finalizada

---

## No PostgreSQL

- Modificação adiada: precisam estar habilitadas em `postgres.conf`
    - Write Ahead Logs (WAL)
    - Point-In-Time Recovery (PITR)

- Modificação imediata:
    - `undo` (rollback)
    - auto-commit precisa estra desabilidado
    - deve executar `commit` ao finalizar

- Backup manual
