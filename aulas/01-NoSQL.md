---
title: NoSQL
author: Leonardo Anjoletto Ferreira
theme: custom
paginate: true
lang: pt-br
---

<!--
_header: CC6240 - Tópicos avançados em banco de dados
_footer: Leonardo Anjoletto Ferreira
-->

# NoSQL

---

# NoSQL

- ACID vs CAP
- Wide-column Storage (Cassandra)
- Document Storage (MongoDB)
- Graph database (Neo4j)

---

## Disponibilidade

- Docker compose com os 3 bancos está no repositório da disciplina
- Free tier de cada um (podem ser usados na disciplina sem nenhum problema):
    - MongoDB: MongoDB Atlas
    - Cassandra: Datastax Astra DB
    - Neo4j: Neo4j AuraDB

---

# ACID vs CAP

<div class="columns">
<div>

**ACID:**
- A: Atomicity
- C: Consistency
- I: Isolation
- D: Durability

</div>
<div>

**CAP:**
- C: Consistency
- A: Availability
- P: Partition Tolerance
* BASE: **B**asic **A**vailability, **S**oft-state, **E**ventual consistency

</div>
</div>

---

![bg h:600px](./figs/CAP.svg)

---

# Document storage

---

## Document storage

- Estrutura dos documentos dependem de como as aplicações representam os dados
    - dados relacionados a um documento são armazenados dentro de apenas um documento (embedded data)
    - são denormalizados
- MongoDB:
    - dados podem ser normalizados (qual é a melhor opção depende do caso)
    - documentos são BSON (JSON binário)

---

## Dados em document storage

<div class="columns">
<div>

Normalizado

![](./figs/mongodb_normalized.png)

</div>
<div>

Embedded

![](./figs/mongodb_embedded.png)

</div>
</div>

<!--_footer: fonte: [MongoDB - Data Modeling Introduction](https://www.mongodb.com/docs/manual/core/data-modeling-introduction/#std-label-manual-data-modeling-intro) -->

---

## Sintaxe da query

```
db.createCollection({"nome"}, {opções})


db.{collection}.{função}({query})

db.{collection}.insertOne({dados})
db.{collection}.insertMany([{dados}])
db.{collection}.find({query})
```

---

## MongoDB vs Álgebra Relacional vs SQL

$\sigma_{\{filtro\}}(\{collection\})$

```db.{collection}.find({filtro})```

```SELECT * FROM {collection} WHERE filtro```

---

## Exemplo

```mongo
db.inventory.find( {
     status: "A",
     $or: [ { qty: { $lt: 30 } }, { item: /^p/ } ]
} )
```

```sql
SELECT *
FROM inventory
WHERE status = "A"
    AND ( qty < 30 OR item LIKE "p%")
```

---

<div class="columns">
<div>

## Vantagens

- Recuperando um documento, todos os dados relacionados ao documento já estão acessíveis
- A operação de escrita é atômica por documento, mesmo que a operação altere mais do que um documento
- Permite que o dado também possa ser normalizado

</div>
<div>

## "Desvantagem"
- Dados não estruturados

</div>
</div>

---

# Wide-column Storage

---

## Empresas que usam

- Cassandra: Activision, Apple, Best Buy, Bloomberg, CERN, Discord, ebay, Hulu, IBM, Instagram, Netflix, The New York Times, Spotify, Target, Uber, Wallmart,

- ScyllaDB: Discord, Epic Games, Comcast, Supercell, **Natura**, GE Digital, Ticketmaster, **Cobli**, happn, **iFood**, mail.ru, Opera

---

## Cassandra vs ScyllaDB

- Scylla é uma reescrita do Cassandra em C++, com algumas otimizações
- Usam a mesma linguagem (CQL), mesmos drivers, mesmos princípios
- Scylla tenta otimizar o uso do hardware para ganhar desempenho


As explicações foram retiradas dos cursos disponibilizados pelo ScyllaDB

---

## CQL

- Linguagem usada para queries em Cassandra e ScyllaDB
- Derivada de SQL, mas sem os joins
- Por isso não vamos passar muito por ela

---

![bg 100%](https://university.scylladb.com/wp-content/uploads/2019/09/relational_vs_scylla-e1594561168311.png)

---

## Arquitetura

Um nó:
- Possui uma parte dos dados
- Todos os nós são iguais
- Divididos em shards (ScyllaDB) ou threads (Cassandra)

Um cluster:
- Feito de nós
- Possui um fator de replicação: número de nós com dados replicados
- Possui um nível de consistência: número de nós que devem reconhecer a leitura ou escrita

---

![bg 50%](https://university.scylladb.com/wp-content/uploads/2019/01/1new.jpg)

---

## Arquitetura

- Dados são armazenados em tabelas (SSTable - Sorted Strings Table)
- Uma coleção de tabelas e seus atributos é chamada de keyspace
- A chave de partição (Partition Key) é usada para distribuir os dados entre os nós
- Os dados são distribuídos em shards (1 shard/hyperthread), sendo que cada shard tem alocado RAM e disco, mantém conexão de rede, gerencia I/O, etc

---

![bg 100%](https://university.scylladb.com/wp-content/uploads/2019/01/2.png)

---

## Linhas e chaves primárias

- Linha (row): unidade que armazena dados e deve conter uma chave primária para identificar a linha dentro da tabela. Os dados dentro de uma linha são armazenados em pares chave-valor em que a chave é o nome da coluna
- Chave primária: uma ou mais colunas que serão usadas para identificar a linha na tabela. A chave primária é obrigatória

---

## Particionamento

- Partição (Partition): coleção de linhas identificadas por uma chave primária única
- Chave de partição (Partition Key): coluna usada para identificar uma linha na tabela e dividir em shards e nós
- Chave de agrupamento (Clustering Key): usada para ordenar as linhas dentro da partição (opcional)
- Chave primária composta: chave de partição + chave de agrupamento

---

## Chave primária = chave de partição

```sql
CREATE TABLE heartrate_v1 (
   pet_chip_id uuid,
   time timestamp,
   heart_rate int,
   PRIMARY KEY (pet_chip_id)
);
```

---

![bg 100%](https://university.scylladb.com/wp-content/uploads/2019/04/primary_key-2-1024x547.png)

---

## Chave de partição + chave de agrupamento

```sql
CREATE TABLE heartrate_v2 (
   pet_chip_id uuid,
   time timestamp,
   heart_rate int,
   PRIMARY KEY (pet_chip_id, time)
);
```

---

## Boas práticas para a escolha da chave primária
1. Partições não são muito pequenas
2. Partições não são grandes
3. Adequadas para as queries
4. Tradeoff de todas as queries esperadas

---

## Materialized Views

- Não funcionam como as views em bancos relacionais
- Cópias de tabelas ordenadas/agrupadas por outras chaves
- As views são populadas com os dados da tabela base
- Os dados não podem ser alterados usando a view, somente a tabela base

---

## Materialized Views

```sql
CREATE TABLE buildings (
    name text,
    city text,
    built int,
    meters int,
    PRIMARY KEY (name)
);
```

```sql
CREATE MATERIALIZED VIEW building_by_city AS
        SELECT * FROM buildings
        WHERE city IS NOT NULL
        PRIMARY KEY(city, name);
```

---

## Últimas definições

- Índices secundários: não são chave de particionamento ou de agrupamento, mas servem para melhorar o desempenho de queries que dependem de dados que não são chaves

- Filtragem: query usando colunas que não são índices secundários ou chaves (não recomendado e é obrigatório passar mais informações para o banco)

---

<div class="columns">
<div>

## Vantagens
- Falta de joins facilitam as queries
- Denormalização faz com que recuperação dos dados seja mais rápida
- Distribuição do banco em várias instâncias/shards: recuperação de falhas, escalabilidade e desempenho


</div>
<div>

## Desvantagens
- Modelagem dos dados é mais complexa
- Modelagem dos dados pode afetar significativamente o tempo de execução da query
- Consistência eventual pode não ser interessante

</div>
</div>

---


# Graph database

---

## Graph database

- Grafo de propriedades que relaciona nós usando vértices
- Tanto nós como vértices possuem propriedades
- Propriedades dos dados não é estruturada
- Normalização dos dados depende da aplicação e pode afetar o tempo de query
- Aplicações com dados heterogêneos com relação de muitos para muitos (e.g., redes sociais)

---

## Conversão de relacional para grafo

<div class="columns">
<div>

Relacional
![](./figs/neo4j-relacional.png)

</div>
<div>

Grafo
![](./figs/neo4j-grafo.png)

</div>
</div>

<!-- _footer: https://neo4j.com/docs/getting-started/data-modeling/relational-to-graph-modeling/-->

---
## Cypher

<div class="columns">
<div>

- Linguagem usada para queries em grafos
- Mistura de ASCII art com SQL:
    - Nós e vértices como ASCII art
    - Restante como SQL
</div>
<div>

![](./figs/neo4j.png)

</div>
</div>

---
## Cypher

<div class="columns">
<div>

```sql
SELECT p.ProductName
FROM Product AS p
JOIN ProductCategory pc ON
(p.CategoryID = pc.CategoryID
AND pc.CategoryName = "Dairy Products")

JOIN ProductCategory pc1 ON
(p.CategoryID = pc1.CategoryID)
JOIN ProductCategory pc2 ON
(pc1.ParentID = pc2.CategoryID
AND pc2.CategoryName = "Dairy Products")

JOIN ProductCategory pc3 ON
(p.CategoryID = pc3.CategoryID)
JOIN ProductCategory pc4 ON
(pc3.ParentID = pc4.CategoryID)
JOIN ProductCategory pc5 ON
(pc4.ParentID = pc5.CategoryID
AND pc5.CategoryName = "Dairy Products");
```

</div>
<div>

```sql
MATCH (p:Product)-[:CATEGORY]->
(l:ProductCategory)-[:PARENT*0..]->
(:ProductCategory {name:"Dairy Products"})
RETURN p.name
```

</div>
</div>

---

## Cypher

```sql
MATCH (tom:Person {name:'Tom Hanks'})-[r]->(m:Movie)
RETURN type(r) AS type, m.title AS movie
```

![h:400px](./figs/neo4j-res-grafo.png)

---

<div class="columns">
<div>

## Vantagens
- Facilita a visualização das relações entre os dados
- Queries podem se tornar mais simples que em bancos relacionais

</div>
<div>

## Desvantagens
- Modelo muito próximo do relacional e pode não compensar a mudança de banco

</div>
</div>
