---
title: Distributed Query Engine
author: Leonardo Anjoletto Ferreira
theme: custom
paginate: true
lang: pt-br
---
<!--
_header: CC6240 - Tópicos avançados em banco de dados
_footer: Leonardo Anjoletto Ferreira
-->

# Distributed Query Engine
## Presto, Trino e Amazon Athena

---

## De uma forma simples

![](./figs/query_dqe.svg)


---

## Distribuição das partes do banco

- Gerenciador: gerencia as queries recebidas e as respostas retornadas
- Worker:
    - parte responsável por processar os dados necessários para retornar as queries
    - recebe a query do gerenciador, busca os dados necessários e resolve a query
- Armazenamento e consulta aos dados: parte responsável por armazenar os dados e também por retornar os dados necessários para que a query seja resolvida

---

## Distributed Query Engine

![](./figs/dqe.svg)

---

## Vantagens

- Divisão do armazenamento e processamento
- O processamento dos dados é feito pelos workers que podem compartilhar dados entre si
- Divisão das queries recebidas em workers
- Criação de workers quando necessário
- Escalar o banco se necessário

---

## Dependendo do serviço usado:
- Permite fazer a query em mais do que um banco ao mesmo tempo (e.g., PostgreSQL + Cassandra)
- Divisão do custo de armazenamento e processamento
- Não precisa necessariamente de um banco de dados para ser usado
- Usa ANSI SQL como linguagem

---

## Presto/PrestoDB

- Projeto iniciado no Facebook (atualmente parte da Linux Foundation)
- Queries em arquivos armazenados em buckets para uso dentro da empresa
- Permite a conexão de diversas fontes de dados para realização de queries

---

## Trino

- Fork do Presto feito por uma parte da equipe original
- Inicio com as mesmas funcionalidades que o Presto
- Atualmente projeto com o mesmo objetivo

---

## Amazon Athena

- Serviço do AWS
- Usa o Trino para fazer as queries

---

## Spark SQL

- Spark não é exatamente um distributed query engine, mas pode ser usado desta forma
- Usa o Spark SQL para executar queries SQL diretamente nos bancos
